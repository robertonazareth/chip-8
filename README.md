Uma reimplementação da máquina virtual Chip-8 escrita em TypeScript, que
permite que programas destinados a esta máquina virtual possam ser executados dentro de
qualquer navegador moderno

Demostração disponível no site: [https://robertonazareth.gitlab.io/chip-8/](https://robertonazareth.gitlab.io/chip-8/)